const checkLoading = async (el) => new Promise(async (resolve, reject) => {
    let loading = document.querySelector(el);
    let i = 0;
    while (typeof (loading) != 'undefined' && loading != null) {
        loading = document.querySelector(el);
        await new Promise(resolve1 => setTimeout(resolve1, 100));
        i++;
        //stop in timeout  10s
        if (Math.floor(((i * 100) % (1000 * 60)) / 1000) > 10) {
            break;
        }
    }
    resolve(Math.floor(((i * 100) % (1000 * 60)) / 1000));
});

(async () => {
    let t = 0;
    //check btn1 mission
    const btn_m1 = document.querySelector("#autoStart");
    if (typeof (btn_m1) != 'undefined' && btn_m1 != null) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        btn_m1.click();
        await new Promise(resolve => setTimeout(resolve, 100));
        t = await checkLoading(`div > div.dialog-content > div > span > img`);
        if (t < 10) {
            t = 0;
            await new Promise(resolve => setTimeout(resolve, 100));
            t = await checkLoading(`div > div.dialog-content > div`);
            if (t < 10) {
                t = 0;
                //check btn2 mission
                const btn_m2 = document.querySelector("#orderDetail > div > div > div.modal-footer.border-0.tabs_btn > div.tabs_btn2.btn.btn-default.btn-rounded.ml-2.shadow");
                if (typeof (btn_m2) != 'undefined' && btn_m2 != null) {
                    await new Promise(resolve => setTimeout(resolve, 1000));
                    localStorage.setItem("statusCode", 200); //OK
                    btn_m2.click();
                } else {
                    localStorage.setItem("statusCode", 404); //element not found
                }
            } else {
                if (document.querySelector("div > div.dialog-content > div").innerText == "A conta tem ordens inacabadas e não pode continuar a receber ordens!") {
                    localStorage.setItem("statusCode", 409); //conflict
                } else if (/Seu nível de associação pode pegar o /.test(document.querySelector("div > div.dialog-content > div").innerText)) {
                    localStorage.setItem("statusCode", 426); //upgrade required
                }
            }
        }
    } else {
        localStorage.setItem("statusCode", 404); //element not found
    }
})();