const puppeteer = require('puppeteer');
const fs = require('fs');
const ini = require('ini');

console.log("Adicione as seguintes lib >> libXcomposite libXtst libXScrnSaver atk at-spi2-atk gtk3");

(async () => {
    let config = {};
    let statusCode = null;
    let currentMission = 0;
    let totalMissions = 0;
    let text = "";
    let d = new Date();
    let c = 0;
    if (fs.existsSync('./config.ini')) {
        config = JSON.parse(JSON.stringify(ini.parse(fs.readFileSync('./config.ini', 'utf-8'))));
    } else {
        let iniText = `[account]\nuser=\npw=\nheadless=false`;
        fs.writeFileSync('./config.ini', iniText);
    }

    if (!fs.existsSync("./img/")) {
        fs.mkdirSync("./img/");
    }

    if (config != null && config.hasOwnProperty("account") && config.account.hasOwnProperty("user") && config.account.hasOwnProperty("pw") && config.account.hasOwnProperty("headless") && config.account.user.length > 0 && config.account.pw.length > 0) {
        const browser = await puppeteer.launch({
            headless: config.account.headless,
            ignoreHTTPSErrors: true,
            devtools: false,
            args: [
                '--disable-web-security',
                '--disable-setuid-sandbox',
                '--no-sandbox',
                '--disable-dev-shm-usage',
                '--disable-accelerated-2d-canvas',
                '--no-first-run',
                '--no-zygote',
                '--disable-gpu'
            ]
        });
        const page = (await browser.pages())[0];
        await page.setUserAgent('Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30');
        await page.goto('https://www.goshopee.app/');
        await page.evaluate((user) => { localStorage.setItem('user', user) }, config.account.user);
        await page.evaluate((pw) => { localStorage.setItem('pw', pw) }, config.account.pw);
        //----------------------Login--------------------------------
        console.log("Realizando Login!");
        await page.addScriptTag({ path: './login.js' });
        //waits for status of script execution
        statusCode = await page.evaluate(() => localStorage.getItem("statusCode"));
        while (statusCode == null) {
            await new Promise(resolve => setTimeout(resolve, 1000));
            statusCode = await page.evaluate(() => localStorage.getItem("statusCode"));
        }
        await page.evaluate(() => localStorage.clear());
        console.log("Login Realizado Com Sucesso. StatusCode: " + statusCode);
        await new Promise(resolve => setTimeout(resolve, 4000));
        if (statusCode != 0) {
            console.log("Error Login: " + statusCode);
            process.exit();
        }
        statusCode = null;
        await new Promise(resolve => setTimeout(resolve, 3000));
        //-----------------------------------------------------------
        //----------------------Print-Screen-Home----------------------------------

        d = new Date();
        if (!fs.existsSync(`./img/welcome/`)) {
            fs.mkdirSync(`./img/welcome/`);
        }
        await page.screenshot({ path: `./img/welcome/${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}-${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}_${d.getHours()}-${d.getMinutes()}-${d.getSeconds()}.png` });


        //---------------------------------------------------
        do {
            if(c > 0){
                d = new Date();
                if (!fs.existsSync(`./img/encomendas/${d.getFullYear()}/${d.getMonth() + 1}/${d.getDate()}/`)) {
                    fs.mkdirSync(`./img/encomendas/${d.getFullYear()}/${d.getMonth() + 1}/${d.getDate()}/`, { recursive: true });
                }
                await page.screenshot({ path: `./img/encomendas/${d.getFullYear()}/${d.getMonth() + 1}/${d.getDate()}/${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}_${d.getHours()}-${d.getMinutes()}-${d.getSeconds()}.png` });
            }
            c++;
            await page.goto('https://www.goshopee.app/index/rot_order/index');
            text = await page.evaluate(() => document.querySelector("body > div.wrapper.homepage > div.swiper-container.card-slide.swiper-container-horizontal.swiper-container-ios > div > div > div.container.z-1.position-relative > div > div > div:nth-child(2) > div.col-12.mt-1 > h3").textContent);
            currentMission = parseInt(text.split("/")[0]);
            totalMissions = parseInt(text.split("/")[1]);
            d = new Date();
            if (!fs.existsSync(`./img/progresso/${d.getFullYear()}/${d.getMonth() + 1}/${d.getDate()}/`)) {
                fs.mkdirSync(`./img/progresso/${d.getFullYear()}/${d.getMonth() + 1}/${d.getDate()}/`, { recursive: true });
            }
            await page.screenshot({ path: `./img/progresso/${d.getFullYear()}/${d.getMonth() + 1}/${d.getDate()}/${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}_${d.getHours()}-${d.getMinutes()}-${d.getSeconds()}.png` });
            if (currentMission < totalMissions) {
                console.log("Missão Atual: " + currentMission + " / " + totalMissions);
                //----------------------do-mission---------------------------
                await page.addScriptTag({ path: './code.js' });
                //waits for status code of script execution
                statusCode = await page.evaluate(() => localStorage.getItem("statusCode"));
                while (statusCode == null) {
                    await new Promise(resolve => setTimeout(resolve, 1000));
                    statusCode = await page.evaluate(() => localStorage.getItem("statusCode"));
                }
                await page.evaluate(() => localStorage.clear());

               
                await new Promise(resolve => setTimeout(resolve, 5000)); //protection time
                
                if (statusCode == 404) {
                    console.log("Error Mission: Element Not Found (" + statusCode + ")");
                    break;
                }
                if (statusCode == 426) {
                    console.log("Todas Encomendas Diarias Foram Completadas");
                    break;
                }
                if (statusCode == 409) {
                    console.log("A conta tem ordens inacabadas e não pode continuar a receber ordens!");
                    break;
                }
                statusCode = null;
                //-----------------------------------------------------------
            } else {
                console.log("Todas Encomendas Diarias Foram Completadas");
                break;
            }
        } while (currentMission < totalMissions);
        console.log("FIM");
        browser.close();
        process.exit();
    } else {
        console.log("Configure o arquivo: config.ini");
    }
})();

