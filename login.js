const checkLoading = async (el) => new Promise(async (resolve, reject) => {
    let loading = document.querySelector(el);
    let i = 0;
    while (typeof (loading) != 'undefined' && loading != null) {
        loading = document.querySelector(el);
        await new Promise(resolve1 => setTimeout(resolve1, 100));
        i++;
        //stop in timeout  10s
        if (Math.floor(((i * 100) % (1000 * 60)) / 1000) > 10) {
            break;
        }
    }
    resolve(Math.floor(((i * 100) % (1000 * 60)) / 1000));
});

(async () => {
    let stat = 0;
    let inp_email = document.querySelector("#inputEmail");
    let inp_pw = document.querySelector("#inputPassword");
    let btn_login = document.querySelector("body > div.wrapper.passport > div:nth-child(3) > div > div.row.mx-0.mt-4 > div > button");
    const user = localStorage.getItem('user');
    const pw = localStorage.getItem('pw');
    localStorage.clear();
    inp_email.value = user;
    inp_pw.value = pw;
    await new Promise(resolve => setTimeout(resolve, Math.random() * (5000 - 1000) + 1000)); //time aleatory
    btn_login.click();
    stat = await checkLoading(`body > div.dialog.dialog-notice.dialog-close > div.dialog-content > div > span`);
    if (stat <= 10) {
        localStorage.setItem('statusCode', 0);
    } else {
        localStorage.setItem('statusCode', 504)
    }

})();