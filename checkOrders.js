(async () => {
    if (localStorage.getItem("position") == null) {
        console.log("obtendo index de missões incompletas!");
        let allInfo = document.querySelectorAll("div.card-header.bg-none > div > div.col-auto > p");
        let pendingMissions = [];
        let c = 0;
        for (const el of [...allInfo]) {
            if (el.classList.contains("badge-warning")) {
                pendingMissions.push(c);
            }
            c++;
            await new Promise(resolve1 => setTimeout(resolve1, 70));
        }
        localStorage.setItem("status", 2);
        localStorage.setItem("obj", JSON.stringify(pendingMissions));
    } else if (localStorage.getItem("position") != null) {
        console.log("Completando missões incompletas pelo index!");
        document.querySelectorAll("div.card-header.bg-none > div > div.col-auto > p")[parseInt(localStorage.getItem("position"))].parentElement.parentElement.parentElement.parentElement.querySelector("button").click();
        await new Promise(resolve1 => setTimeout(resolve1, 1000));
        document.querySelector("#orderDetail > div > div > div.modal-footer.border-0.tabs_btn > div.tabs_btn2.btn.btn-default.btn-rounded.ml-2.shadow").click();
        await new Promise(resolve1 => setTimeout(resolve1, 5000));
        localStorage.setItem("status", 2);
    }
})();